import { Component, OnInit } from '@angular/core';
import {UserService } from '../../shared_service/user.service';
import  {User }  from '../../user';
import{Router} from '@angular/router';
import { Voiture } from '../../voiture';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-update-compte',
  templateUrl: './update-compte.component.html',
  styleUrls: ['./update-compte.component.scss']
})
export class UpdateCompteComponent implements OnInit {
  users:any;
  user =new User;
  submitted=false;
  message:String;
  id:any;
  listca:any;
  array:any;
  constructor(private userService:UserService,private router:Router,private toastr: ToastrService) { }

  ngOnInit() {

      
       this.userService.getUsersmd().subscribe(data=>{this.users=data;  });
    
  
  }


  deleteUser(user){
    console.log("user user user"+ user);
    this.userService.deleteUser(user.id).subscribe(result=>
    
    this.toastr.success("Suppression effectué avec succés "+localStorage.getItem('token')));
    location.reload();
  
    //location.reload();
  }



  
  updateUser(user){
    console.log("user user user"+ user.id);
    this.userService.setter(user);
    this.router.navigate(['/tomodify']);
    this.userService.updateUser(user);
    //this.userService.getUsers().subscribe(data=>{this.users=data;});

  }


  newUser(){
    let user=new User();
      this.userService.setter(user);
      this.router.navigate(['/compte']);
      this.userService.getUsers().subscribe(data=>{this.array=data;});
    }
}
