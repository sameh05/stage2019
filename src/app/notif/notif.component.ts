import { Component, OnInit } from '@angular/core';
import  {User }  from '../user';
import{Router} from '@angular/router';
import { Voiture } from '../voiture';
import {UserService } from '../shared_service/user.service';

@Component({
  selector: 'app-notif',
  templateUrl: './notif.component.html',
  styleUrls: ['./notif.component.scss']
})

export class NotifComponent implements OnInit {
  users:Array<any>;
  user =new User;
  submitted=false;
  message:String;
  id:any;
  listca:any;
  array:any;
  array2:any;
  i:Number;
  demande:any;
  idd:any;
  ud:any;
  constructor(private userService:UserService,private router:Router) { }

  ngOnInit() {


    
    this.userService.getUsers().subscribe(data=>{this.listca=data;
      for (var i = 0; i < this.listca.length; i++) {
     this.array2=this.listca[i].absences;
      console.log(this.array+"here listca");
            
    }
      console.log(this.listca);});
       this.id=localStorage.getItem('token');
       //this.userService.getUsersca(this.id).subscribe(data=>{this.users=data;  });
     //  this.userService.createdemandeid(this.id ).subscribe(data=>{this.id=data;
      this.userService.Uv1().subscribe(data=>{this.array=data;
      console.log(this.array.absences+"here array");
    
      
      })
  //  } );
  this.demande=this.userService.getterdemande();
  this.idd=this.userService.getterid();
  console.log(this.idd+"ididid");
  this.demande=this.userService.getterdemande();
  this.idd=this.userService.getterid();
  console.log(this.idd+"ididid");
  this.userService.getUser(this.idd).subscribe(data=>{ this.ud=data ;console.log(this.ud);})
  }


  deleteUser(user){
    this.userService.deleteUser(user.id).subscribe(result=>console.log(result));
   
    this.userService.getUsers().subscribe(data=>{this.users=data;});
  }

  deletedemande1(demande){
    this.userService.deletedemande1(demande.id).subscribe(result=>console.log(result))
    this.router.navigate(["op2"]);
    location.reload();
    
    
    
  
    this.userService.getUsers().subscribe(data=>{this.users=data;});
  }
  getUser(user){
    this.userService.getUser(user.id).subscribe(result=>console.log(result));
  }
  
  updateUser(user){
    this.userService.setter(user);
    this.router.navigate(['/rp3']);
    this.userService.updateUser(user);
    this.userService.getUsers().subscribe(data=>{this.users=data;});
    
  }


  updatedemande(demande , ind){
    console.log(ind);
    this.userService.setter1(demande);
    this.userService.setter11(ind);
    
    this.router.navigate(['/rp3']);
  

   // this.userService.setter1(demande);
    //this.router.navigate(['/op']);
  
    //this.userService.updatedemande(demande);
    this.id=localStorage.getItem('token');
    this.userService.Uv(this.id).subscribe(data=>{this.array=data;});
  }
  newUser(){
  let user=new User();
    this.userService.setter(user);
    this.router.navigate(['/op']);
    this.userService.getUsers().subscribe(data=>{this.array=data;});
  }
  newVoiture(){
    let voiture=new Voiture();
      this.userService.setter3(voiture);
      this.router.navigate(['/manyo']);
      
      this.userService.getUsers().subscribe(data=>{this.users=data;});
    }

}
