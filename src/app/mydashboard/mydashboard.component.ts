import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointState, BreakpointObserver } from '@angular/cdk/layout';
import *as ChartConst from 'ng6-o2-chart';
import {UserService } from '../shared_service/user.service';
import  {User }  from '../user';
import{Router} from '@angular/router';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-mydashboard',
  templateUrl: './mydashboard.component.html',
  styleUrls: ['./mydashboard.component.css']
})
export class MydashboardComponent {
  /** Based on the screen size, switch from standard to one column per row */
  
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: 'Card 1', cols: 1, rows: 1 },
          { title: 'Card 2', cols: 1, rows: 1 },
          { title: 'Card 3', cols: 1, rows: 1 },
          { title: 'Card 4', cols: 1, rows: 1 }
        ];
      }

      return [
        { title: 'Card 1', cols: 2, rows: 1 },
        { title: 'Card 2', cols: 1, rows: 1 },
        { title: 'Card 3', cols: 1, rows: 2 },
        { title: 'Card 4', cols: 1, rows: 1 }
      ];



      
    })
  );
  L1: any[] = [];
  L2: any[] = [];
  L4: any[] = [];
  L5: any[] = [];
  L6:any[]=[];
  L7:any[]=[];
  LL1: any[] = [];
  LL2: any[] = [];
  LL4: any[] = [];
  LL5: any[] = [];
  LL6:any[]=[];
  LL7:any[]=[];
  L3:Array<any>;
  users:any;
nb1:number;
nb2:number;
  nb:number;
  nb3:number;
  nb4:number;
  nb5:number;
  n6:number;
  n7:number;
  n8:number;
  n9:number;
  n10:number;
  n11:number;
  n12:number;
  n13:number;
  n14:number;
  n15:number;
  n16:number;
  n17:number;
  n18:number;
  n19:number;


  a1:number;
a2:number;
  a3:number;
  a4:number;
  
  a5:number;
  a6:number;
  a7:number;
  a8:number;
  a9:number;
  a10:number;
  a11:number;
  a12:number;
  a13:number;
  a14:number;
  a15:number;
  a16:number;
  a17:number;
  a18:number;
  a19:number;

  p1:number;
  p6:number;
  p7:number;
  p8:number;
  p9:number;
  p10:number;
  p11:number;
  p12:number;
  p13:number;
  p14:number;
  p15:number;
  p16:number;
  p17:number;
  p18:number;
  p19:number;
ss1:number;
ss2:number;
ss3:number;
  email1:String;
  email: string;
  model:any={};
  ListPresencedash:any;
  ListPresence4:any;
  ListPresence3:any;
  dash:any;
  b1:string;
  b2:string;
  b3:string;
  b4:string;
  b5:string;
  b6:string;
  iff:string;
  date1:any;
  date2:any;
  dated1:any;
  dated2:any;
  ipp1:number;
  ipp2:number;
  nbbb:number;
  nbb1:number;
  st1:string;
  st2:string;
  st3:string;
  st4:number;
  st5:number;
  st6:number;
  stt1:number;
  stt2:number;
  stt3:string;
  stt4:string;
  b :boolean;
  sttt:number;
  sttt1:number;
  ids :number;
  idst1:number;
idst2:number;
idst3:number;
u6 :number;
u1:number;
u2:number;
u3:number;
u4:number;
u5:number;
uu1:string;
uu2:string;
uu3:string;
uu4:string;
uu5:string;
uu6:string;
c1:number;
c2:number;
c3:number;
c4:number;
c5:number;
cc1:string;
cc2:string;
cc3:string;
cc4:string;
cc5:string;
cc6:string;
bc :boolean;
  constructor(private userService:UserService,private router:Router,private breakpointObserver: BreakpointObserver,private datePipe: DatePipe) { this.barTypeName       = ChartConst.LINE_CHART_TYPE_NAME;
    this.lineTypeName     	= ChartConst.LINE_CHART_TYPE_NAME;
    this.barTypeName     	= ChartConst.BAR_CHART_TYPE_NAME;
    this.pieTypeName     	= ChartConst.PIE_CHART_TYPE_NAME;
    this.scatterPlotTypeName 	= ChartConst.SCATTER_PLOT_CHART_TYPE_NAME;
    this.histogramTypeName     = ChartConst.HISTOGRAM_CHART_TYPE_NAME;
    this.stackBarTypeName     = ChartConst.STACK_BAR_CHART_TYPE_NAME;
    this.geoMapTypeName     = ChartConst.GEO_MAP_CHART_TYPE_NAME;
    this.geoOrthographicTypeName= ChartConst.GEO_ORTHOGRAPHIC_CHART_TYPE_NAME;
    this.treeMapTypeName     = ChartConst.TREE_MAP_CHART_TYPE_NAME;
    this.packLayoutTypeName 	= ChartConst.PACK_LAYOUT_CHART_TYPE_NAME;
    this.choroplethTypeName 	= ChartConst.CHOROPLETH_CHART_TYPE_NAME;
    this.treeTypeName     	= ChartConst.TREE_CHART_TYPE_NAME;
    this.forceTypeName     	= ChartConst.FORCE_CHART_TYPE_NAME;

    this.initilizeData();
    
    this.userService.getUsers().subscribe(data=>{console.log(data);
      this.listca=data;
      console.log(this.listca);
      console.log(this.listca[1].id);
      //console.log(this.listca[5].absences[0].dated);
      console.log(this.listca.length);
     // for (var i = 0; i < this.listca.length; i++) {

      //  this.array2=this.listca[i].absences;
        // console.log(this.array2);
               
       //}
         console.log(this.listca);});} ;
         sub(fonction1 , fonction2){
          this.date1 = this.datePipe.transform(fonction1, 'yyyy-MM-dd');
          this.date2 = this.datePipe.transform(fonction2, 'yyyy-MM-dd');
          this.p19=0 ;
          this.ss1=0;
          this.ipp1=0;
          this.ipp2=0;
          for (this.nbbb = 0; this.nbbb < this.listca.length; this.nbbb++) {
            console.log(this.listca[this.nbbb]);
           this.iff=this.listca[this.nbbb].type;
           if(this.listca[this.nbbb] !=null ){
             for(this.nbb1=0 ;this.nbb1<this.listca.length ; this.nbb1++){
               if(this.listca[this.nbb1].type == this.iff ){

               
             console.log(this.listca[this.nbb1].dated+"jjjjjjjjjjjjjj");
             console.log((new Date(this.listca[this.nbb1].dated).getTime()));
             console.log( (new Date(this.date1)).getTime());
             console.log(( new Date(this.listca[this.nbb1].dated)).getTime()>= ( (new Date(this.date1)).getTime()))
             const _MS_PER_DAY = 1000 * 60 * 60 * 24;

             let newDate = new Date(this.listca[this.nbb1].datef);
              let newDate1 = new Date(this.listca[this.nbb1].dated);
             if((newDate1).getTime()>= new Date(this.date1).getTime() 
             &&(newDate).getTime()<= new Date(this.date2).getTime() 
          
             ){
               
               console.log(this.listca[this.nbbb].id);
              
              const utc1 = Date.UTC(newDate1.getFullYear(), newDate1.getMonth(), newDate1.getDate());
              const utc2 = Date.UTC(newDate.getFullYear(), newDate.getMonth(), newDate.getDate());
             this.ipp1=Math.floor((utc2 - utc1) / _MS_PER_DAY);
              console.log(this.ipp1+"re");
 

              console.log(this.ipp1+"heeeey dateee");
             }}
             
             }
            this.L2.push(this.listca[this.nbbb].type);
              this.L1.push(this.ipp1);
        
              console.log(this.L1+"*****"+this.L2);

            }
           } console.log(this.L2+"daaaaaaaaaaaaaaaaaaaaaaaad");
           for (this.st4 = 0; this.st4 < this.L2.length; this.st4++) {
            this.st6=0;
           
           this.st1=this.L2[this.st4];
           console.log(this.st1+"st11111111111");
           if(this.L2[this.st4] !=null ){
             for(this.st5=0 ;this.st5<this.L2.length ; this.st5++){
               if(this.L2[this.st5] == this.st1 ){
                 this.st6= this.st6 + this.L1[this.st5];

               }
              
              }
           
            } console.log(this.st1); 
            console.log(this.st6);
            this.L4.push(this.st1);
            this.L5.push(this.st6);
            console.log(this.L4+"%%%%%%%%%%%%%%%"+this.L5);
          
          }



          this.L7.push(this.L5[0]);
          this.L6.push(this.L4[0]);
          for (this.stt1 = 1; this.stt1 < this.L4.length; this.stt1++) {
           this.b=false;
           this.stt4=this.L4[this.stt1]; 
             for(this.stt2=0 ;this.stt2<this.L6.length ; this.stt2++){
               if(this.L6[this.stt2] == this.stt4){
                 this.b=true;    
              }
            } 
            if(this.b == false){ this.L7.push(this.L5[this.stt2]);
            this.L6.push(this.L4[this.stt2]);}
            console.log(this.L6+"8888888888888888888888888888888"+this.L7);  
          }

          this.sttt=0;
          for(this.sttt1=0 ;this.sttt1<this.L7.length ; this.sttt1++){
               this.sttt=this.sttt+this.L7[this.sttt1];
           



         } 



           this.pieDataJson =
    {
    	'data':[
        {
          'name': this.L6[0],
          'value':Math.abs(this.L7[0]) ,
        },
        {
          'name': this.L6[1],
          'value':Math.abs(this.L7[1]),
        },
        {
          'name': this.L6[2],
          'value':Math.abs(this.L7[2]),
        },
        {
          'name': this.L6[3],
          'value':Math.abs(this.L7[3]),
        },
        {
          'name': this.L6[4],
          'value':Math.abs(this.L7[4]),
        },
        {
          'name': this.L6[5],
          'value':Math.abs(this.L7[5]),
        },
        {
          'name': this.L6[6],
          'value':Math.abs(this.L7[6]),
        },
        {
          'name': this.L6[7],
          'value':Math.abs(this.L7[7]),
        },
        {
          'name': this.L6[8],
          'value':Math.abs(this.L7[8]),
        },
        {
          'name': this.L6[9],
          'value':Math.abs(this.L7[9]),
        },
    	],
    };
           






    this.pieDataJson =
    {
    	'data':[
        {
          'name': this.L6[0]+' '+Math.abs(this.L7[0]),
          'value': Math.abs(this.L7[0])*100/this.sttt, 
        },
        {
          'name': this.L6[1]+' '+Math.abs(this.L7[1]),
          'value': Math.abs(this.L7[1])*100/this.sttt,
        },
        {
          'name': this.L6[2]+' '+Math.abs(this.L7[2]),
          'value': Math.abs(this.L7[2])*100/this.sttt,
        },
        {
          'name': this.L6[3]+' '+Math.abs(this.L7[3]),
          'value':Math.abs(this.L7[3])*100/this.sttt,
        },
        {
          'name': this.L6[4]+' '+Math.abs(this.L7[4]),
          'value': Math.abs(this.L7[4])*100/this.sttt,
        },
        {
          'name': this.L6[5]+' '+Math.abs(this.L7[5]),
          'value': Math.abs(this.L7[5])*100/this.sttt,
        },
        {
          'name': this.L6[6]+' '+Math.abs(this.L7[6]),
          'value': Math.abs(this.L7[6])*100/this.sttt,
        },
        {
          'name': this.L6[7]+' '+Math.abs(this.L7[7]),
          'value': Math.abs(this.L7[7])*100/this.sttt,
        },
        {
          'name': this.L6[8]+' '+Math.abs(this.L7[8]),
          'value': Math.abs(this.L7[8])*100/this.sttt,
        },
        {
          'name': this.L6[9]+' '+Math.abs(this.L7[9]),
          'value': (Math.abs(this.L7[9])/this.sttt)*100,
        },
    	],
    };
           
                   
                   
           
      //console.log(this.L1[1]);   
      //console.log(this.L1[2]); 
      //console.log(this.L1[3]); 
      //console.log(this.L1[4]); 
  //this.p1=this.L1[1];
  //this.p6=this.L1[2];
  //this.p7=this.L1[3];
  //this.p8=this.L1[4];
  //this.p9=this.L1[5];
  //this.p10=this.L1[6];
  //this.p11=this.L2[1];
  //this.p12=this.L2[2];
  //this.p13=this.L2[3];
  //this.p14=this.L2[4];
  //this.p15=this.L2[5];
  //this.p16=this.L2[6];

         }

     



















  chartType:string;
  configData:any;
  barDataJson:any;

  geoMapDataJson:any;
  geoOrthographicDataJson:any;
  choroplethDataJson:any;
  scatterPlotDataJson:any;
  lineDataJson:any;
  histogramDataJson:any;
  pieDataJson:any;
  packLayoutDataJson:any;
  treeMapDataJson:any;
  stackBarDataJson:any;
  treeDataJson:any;
  forceDataJson:any;
  DataSetJson:string;

  lineTypeName:string;
  barTypeName: string;
  pieTypeName:string;
  scatterPlotTypeName:string;
  histogramTypeName:string;
  stackBarTypeName:string;
  geoMapTypeName:string;
  geoOrthographicTypeName:string;
  treeMapTypeName:string;
  packLayoutTypeName:string;
  choroplethTypeName:string;
  treeTypeName:string;
  forceTypeName:string;

  ListPresence:Array<any>;
 
  listca:any;
  array:any;
  array2:any;
  updateUd(email1 , id){


    this.userService.getUsersmd().subscribe(data=>{this.users=data;  });
    this.userService.getUsers().subscribe(data=>{this.listca=data;
      for (var i = 0; i < this.listca.length; i++) {
     this.array2=this.listca[i].absences;
      console.log(this.array);
            
    }})
    //this.email=localStorage.getItem('token');

    this.email=email1;
    this.ids=id;
    console.log(this.email)+"email";
    console.log(localStorage.getItem('token'));
    window.alert("okk");
    console.log(JSON.stringify(this.model));
    this.userService.createUserLog1().subscribe ( (ListPresence)=>{console.log(ListPresence);
      if(ListPresence){
      console.log(ListPresence+"ahahahahahhahahahahahahahahha**************");
      console.log(parseInt("12545"));
      console.log(ListPresence.length);
      this.nb1=0;
      this.nb=0;
      this.nb2=0;
      this.nb3=0;
      this.nb4=0;
      this.nb5=0;
      this.n6=0;
      this.n7=0;
      this.n8=0;
      this.n9=0;
      this.n10=0;
      this.n11=0;
      this.n12=0;
      this.n13=0;
      this.n14=0;
      this.n15=0;
      this.n16=0;
      this.n17=0;
      this.n18=0;
      this.n19=0;
      
      for(this.nb ; this.nb<ListPresence.length; this.nb++){
        if((ListPresence[this.nb]=="12")){
console.log("yeeeeeeeeeees");
this.nb1=this.nb1+1;
console.log(this.nb1);
        };    
        if((ListPresence[this.nb]=="11")){
          console.log("yeeeeeeeeeees");
          this.nb3=this.nb3+1;
          console.log(this.nb3);
                  };      
                  if((ListPresence[this.nb]=="10")){
                    console.log("yeeeeeeeeeees");
                    this.nb4=this.nb4+1;
                    console.log(this.nb4);
                            };    
                            if((ListPresence[this.nb]=="9")){
                              console.log("yeeeeeeeeeees");
                              this.nb5=this.nb5+1;
                              console.log(this.nb5);
                                      };    
                                      if((ListPresence[this.nb]=="1")){
                                        console.log("yeeeeeeeeeees");
                                        this.n6=this.n6+1;
                                        console.log(this.n6);
                                                };    
                                                if((ListPresence[this.nb]=="2")){
                                                  console.log("yeeeeeeeeeees");
                                                  this.n7=this.n7+1;
                                                  console.log(this.n7);
                                                          };            if((ListPresence[this.nb]=="3")){
                                                            console.log("yeeeeeeeeeees");
                                                            this.n8=this.n8+1;
                                                            console.log(this.n8);
                                                                    };           
                                                                    if((ListPresence[this.nb]=="4")){
                                                                      console.log("yeeeeeeeeeees");
                                                                      this.n9=this.n9+1;
                                                                      console.log(this.n9);
                                                                              };           
                                                                              if((ListPresence[this.nb]=="5")){
                                                                                console.log("yeeeeeeeeeees");
                                                                                this.n10=this.n10+1;
                                                                                console.log(this.n10);
                                                                                        };   
                                                                                        
      }this.nb2=this.nb1;
      this.n11=this.nb3;
      this.n12=this.nb4;
      this.n13=this.nb5;
      this.n14=this.n6;
      this.n15=this.n7;
      this.n16=this.n8;
      this.n17=this.n9;
      this.n18=this.n10;

      console.log(this.nb2+"ytytyyyyyyyyyyyyyyy");
     
     /* this.barDataJson =
      {
        'series':[
          
          'Congés'
        ],
        'data':[
          {
            'x': 'sep',
            'y': [10*this.n18],
          },
          {
            'x': 'oct',
            'y': [10*this.n12],
          },
          {
            'x': 'nov',
            'y': [10*this.n11,],
          },
          {
            'x': 'dec',
            'y': [10*this.nb2],
          },
          {
            'x': 'janv',
            'y': [10*this.n14],
          },
          {
            'x': 'fev',
            'y': [10*this.n15],
          },
          {
            'x': 'mars',
            'y': [10*this.n16],
          },
          {
            'x': 'avril',
            'y': [10*this.n17],
          },
          {
            'x': 'mai',
            'y': [10*this.n18],
          },
          {
            'x': 'juin',
            'y': [10*this.n18],
          },
          {
            'x': 'juillet',
            'y': [10*this.n18],
          },
          {
            'x': 'aout',
            'y': [10*this.n18],
          },
        ],
      };*/
      
      
    
     } 
    
    })




    this.userService.createUserLog1l(this.email).subscribe ( (ListPresence)=>{console.log(ListPresence);
  
    
 
for(this.idst1=0 ;this.idst1 < this.ListPresence4.length ;this.idst1++){
  if(this.ListPresence4[this.idst1].id == this.ids ){
  for (this.u1 = 0; this.u1 < this.ListPresence4[this.idst1].absences.length; this.u1++) {
    console.log( this.ListPresence4[this.idst1].absences[this.u1]);
   this.uu1= this.ListPresence4[this.idst1].absences[this.u1].type;
   if( this.ListPresence4[this.idst1].absences[this.u1] !=null ){
     for(this.u2=0 ;this.u2< this.ListPresence4[this.idst1].absences.length ; this.u2++){
       if( this.ListPresence4[this.idst1].absences[this.u2].type == this.uu1 ){
  
       
     console.log( this.ListPresence4[this.idst1].absences[this.u2].dated+"jjjjjjjjjjjjjj");
     console.log((new Date( this.ListPresence4[this.idst1].absences[this.u2].dated).getTime()+"brrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrriiiiiiiiiiiimmmaa"));
     console.log( (new Date(this.date1)).getTime());
     console.log(( new Date( this.ListPresence4[this.idst1].absences[this.u2].dated)).getTime()>= ( (new Date(this.dated1)).getTime()))
     const _MS_PER_DAY1 = 1000 * 60 * 60 * 24;
  
     let newDated = new Date( this.ListPresence4[this.idst1].absences[this.u2].datef);
      let newDated1 = new Date( this.ListPresence4[this.idst1].absences[this.u2].dated);
    
       
       console.log( this.ListPresence4[this.idst1].absences[this.u2].id);
      
      const utcd1 = Date.UTC(newDated1.getFullYear(), newDated1.getMonth(), newDated1.getDate());
      const utcd2 = Date.UTC(newDated.getFullYear(), newDated.getMonth(), newDated.getDate());
     this.u3=Math.floor((utcd2 - utcd1) / _MS_PER_DAY1);
      console.log(Math.floor((utcd2 - utcd1) / _MS_PER_DAY1)+"rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrre");
      console.log(this.u3+"heeeey dateee$$$$$$$$$$$$$$$$$$*****************");
     }
     
     }
    this.LL2.push( this.ListPresence4[this.idst1].absences[this.u1].type);
      this.LL1.push(this.u3);
  
      console.log(this.LL1+"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"+this.LL2);
  
    }
   }} }
   
   console.log(this.LL2+"daaaaaaaaaaaaaaaaaaaaaaaad");
   for (this.u4 = 0; this.u4 < this.LL2.length; this.u4++) {
    this.u6=0;
   
   this.uu2=this.LL2[this.u4];
   console.log(this.uu2+"st11111111111");
   if(this.LL2[this.u4] !=null ){
     for(this.u5=0 ;this.u5<this.LL2.length ; this.u5++){
       if(this.LL2[this.u5] == this.uu2 ){
         this.u6= this.u6 + this.LL1[this.u5];
  
       }
      
      }
   
    } console.log(this.st1); 
    console.log(this.st6);
    this.LL4.push(this.uu2);
    this.LL5.push(this.u6);
    console.log(this.LL4+"TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"+this.LL5);
  
  }
  
  
  
  this.LL7.push(this.LL5[0]);
  this.LL6.push(this.LL4[0]);
  for (this.c1 = 1; this.c1 < this.LL4.length; this.c1++) {
   this.bc=false;
   this.cc2=this.LL4[this.c1]; 
     for(this.c2=0 ;this.c2<this.LL6.length ; this.c2++){
       if(this.LL6[this.c2] == this.cc2){
         this.bc=true;    
      }
    } 
    if(this.bc == false){ this.LL7.push(this.LL5[this.c2]);
    this.LL6.push(this.LL4[this.c2]);}
    console.log(this.LL6+"8888888888888888888888888888888"+this.LL7);  
  }
  
  this.c3=0;
  for(this.c4=0 ;this.sttt1<this.LL7.length ; this.c4++){
       this.c3=this.c3+this.LL7[this.c4];
   
  
  
  
  } 
  

      this.barDataJson =
      {
        
        'series':[
          'participation',
          
        ],
        'data':[
          {
            'x': this.LL6[0],
            'y': [this.LL7[0] , 0],
          },
          {
            'x': this.LL6[1],
            'y': [this.LL7[1] , 0],
          },
          {
            'x': this.LL6[2],
            'y': [this.LL7[2] , 0],
          },
          {
            'x': this.LL6[3],
            'y': [this.LL7[3] , 0],
          },
          {
            'x': this.LL6[4],
            'y': [this.LL7[4] , 0],
          },
          {
            'x': this.LL6[5],
            'y': [this.LL7[5] , 0],
          },
          {
            'x': this.LL6[6],
            'y': [this.LL7[6] , 0],
          },
        
         
        
        ],
      };
      
      
    
     
    
    })




    // ConfigData = this.httpClient.get('assets/json/ConfigData.json');
    this.configData = {
      // tslint:disable-next-line:quotemark
      "className": {
        'axis': 'axis',
        'axisXBorder': 'axis_x',
        'axisXText': 'axis-x-text',
        'bar': 'bar',
        'barValue': 'bar-value',
        'line': 'line',
        'multiLinePrefix': 'line-',
        'grid': 'grid',
        'pie': 'pie',
        'pieInnerTitle': 'pie-inner-title',
        'pieInnerRadius': 'total',
        'histogram': 'histogram',
        'histogramBar': 'histogram-bar',
        'treemap': 'treemap',
        'treemapLabel': 'treemap-label',
        'packlayout': 'packlayout',
        'packlayoutLabel': 'packlayout-label',
      },
      'label': {
          'display': true,
      },
      'title': {
        'display': true,
        'name': 'taux de participation  ',
        'className': 'chart-title',
        'height': 30,
        'leftMargin': -20,
        'bottomMargin': 10
      },
      'maxValue': {
        'auto': true,
        'x': 100,
        'y': 100,
      },
      'legend': {
        'display': true,
        'position':           'right',
        'totalWidth': 80,
        'initXPos': 5,
        'initYPos': 10,
        'rectWidth': 10,
        'rectHeight': 10,
        'xSpacing': 2,
        'ySpacing': 2
      },
      'color': {
        'auto': true, //
        'defaultColorNumber': 10,
        'opacity': 1.0,
        'userColors': [
          'blue',
          'red',
          'green',
          'yellow',
          'PaleGoldenrod',
          'Khaki',
          'DarkKhaki',
          'Gold',
          'Cornsilk',
          'BlanchedAlmond',
          'Bisque',
          'NavajoWhite',
          'Wheat',
          'BurlyWood',
          'Tan',
          'RosyBrown',
          'SandyBrown',
          'Goldenrod',
          'DarkGoldenrod',
          'Peru',
          'Chocolate'
        ],
        'focusColor': 'red',
      },
      'pie': {
        'innerRadius': {
          'percent': 20,
          'title': 'Total'
        },
        'value': {
          'display': true,
        },
        'percent':{
          'display': false,
        }
      },
      'line': {
        'legend': 'lineEnd',
        'interpolate' : 'linear',
      },
      'grid': {
        'x': {
          'display': true,
        },
        'y':{
          'display': true,
        },
      },
      'margin': {
        'top': 30,
        'left': 30,
        'right': 10,
        'bottom': 20,
        'between': 5
      },
      'axis': {
        'rotation': 0,
        'borderLineWidth': 1,
        'xLabel': {
          'leftMargin': 0,
          'bottomMargin': 5
        },
        'yLabel':{
          'leftMargin': 0,
          'bottomMargin': 0
        },
      },
      'animation':{
        'enable':true,
        'duration':4000,
      },
    };
    this.userService.getUsers().subscribe(data=>{this.listca=data;
      for (var i = 0; i < this.listca.length; i++) {
     this.array2=this.listca[i].absences;
      console.log(this.array);
            
    }})
    this.email=localStorage.getItem('token');
    console.log(localStorage.getItem('token'));
    this.userService.createUserLog1().subscribe ( (ListPresence3)=>{console.log(ListPresence3+"liiist3333");
    this.ListPresence4=ListPresence3;})
    this.userService.createUserLog2dash(this.email).subscribe ( (ListPresencedash)=>{console.log(ListPresencedash+"noooooooooo1");

this.dash=ListPresencedash;
this.a1=0;
this.a2=0;
this.a3=0;
this.a4=0;
this.a5=0;
this.a6=0;
this.a7=0;
this.a8=0;
this.a9=0;
this.a10=0;
this.a11=0;
this.a12=0;
this.a13=0;
this.a14=0;
this.a15=0;
this.a16=0;
this.a17=0;
this.a18=0;
this.a19=0;
this.b1=null;
this.b2=null;
this.u3=0;
console.log(this.ListPresence4[1].absences[0].dated +"liiiiiiiiiiiiiste4444444444444444444");


for(this.idst1=0 ;this.idst1 < this.ListPresence4.length ;this.idst1++){
if(this.ListPresence4[this.idst1].id == this.ids ){
for (this.u1 = 0; this.u1 < this.ListPresence4[this.idst1].absences.length; this.u1++) {
  console.log( this.ListPresence4[this.idst1].absences[this.u1]);
 this.uu1= this.ListPresence4[this.idst1].absences[this.u1].type;
 if( this.ListPresence4[this.idst1].absences[this.u1] !=null ){
   for(this.u2=0 ;this.u2< this.ListPresence4[this.idst1].absences.length ; this.u2++){
     if( this.ListPresence4[this.idst1].absences[this.u2].type == this.uu1 ){

     
   console.log( this.ListPresence4[this.idst1].absences[this.u2].dated+"jjjjjjjjjjjjjj");
   console.log((new Date( this.ListPresence4[this.idst1].absences[this.u2].dated).getTime()+"brrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrriiiiiiiiiiiimmmaa"));
   console.log( (new Date(this.date1)).getTime());
   console.log(( new Date( this.ListPresence4[this.idst1].absences[this.u2].dated)).getTime()>= ( (new Date(this.dated1)).getTime()))
   const _MS_PER_DAY1 = 1000 * 60 * 60 * 24;

   let newDated = new Date( this.ListPresence4[this.idst1].absences[this.u2].datef);
    let newDated1 = new Date( this.ListPresence4[this.idst1].absences[this.u2].dated);
  
     
     console.log( this.ListPresence4[this.idst1].absences[this.u2].id);
    
    const utcd1 = Date.UTC(newDated1.getFullYear(), newDated1.getMonth(), newDated1.getDate());
    const utcd2 = Date.UTC(newDated.getFullYear(), newDated.getMonth(), newDated.getDate());
   this.u3=Math.floor((utcd2 - utcd1) / _MS_PER_DAY1);
    console.log(Math.floor((utcd2 - utcd1) / _MS_PER_DAY1)+"rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrre");
    console.log(this.u3+"heeeey dateee$$$$$$$$$$$$$$$$$$*****************");
   }
   
   }
  this.LL2.push( this.ListPresence4[this.idst1].absences[this.u1].type);
    this.LL1.push(this.u3);

    console.log(this.LL1+"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"+this.LL2);

  }
 }} }
 
 console.log(this.LL2+"daaaaaaaaaaaaaaaaaaaaaaaad");
 for (this.u4 = 0; this.u4 < this.LL2.length; this.u4++) {
  this.u6=0;
 
 this.uu2=this.LL2[this.u4];
 console.log(this.uu2+"st11111111111");
 if(this.LL2[this.u4] !=null ){
   for(this.u5=0 ;this.u5<this.LL2.length ; this.u5++){
     if(this.LL2[this.u5] == this.uu2 ){
       this.u6= this.u6 + this.LL1[this.u5];

     }
    
    }
 
  } console.log(this.st1); 
  console.log(this.st6);
  this.LL4.push(this.uu2);
  this.LL5.push(this.u6);
  console.log(this.LL4+"TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"+this.LL5);

}



this.LL7.push(this.LL5[0]);
this.LL6.push(this.LL4[0]);
for (this.c1 = 1; this.c1 < this.LL4.length; this.c1++) {
 this.bc=false;
 this.cc2=this.LL4[this.c1]; 
   for(this.c2=0 ;this.c2<this.LL6.length ; this.c2++){
     if(this.LL6[this.c2] == this.cc2){
       this.bc=true;    
    }
  } 
  if(this.bc == false){ this.LL7.push(this.LL5[this.c2]);
  this.LL6.push(this.LL4[this.c2]);}
  console.log(this.LL6+"8888888888888888888888888888888"+this.LL7);  
}

this.c3=0;
for(this.c4=0 ;this.sttt1<this.LL7.length ; this.c4++){
     this.c3=this.c3+this.LL7[this.c4];
 



} 







for(this.a1 ; this.a1<this.ListPresence4.length; this.a1++){
  console.log(this.ListPresence4.length);
  console.log(this.dash.length);
  if((this.ListPresence4[this.a1].email== this.email)){
    console.log(this.email+"kakkkkaaaaaaaaaaaaaaaaaaaaaaaaa$$$$$$$$$$$$$$$$");
    if((this.dash[this.a1]=="mat")){
      console.log("mattttt");
      this.a2=this.a2+1;
      console.log("mattttt2222222227777777");
      console.log(this.a2);

    }
    if((this.dash[this.a1]=="maln")){
      this.a3=this.a3+1;
      console.log(this.a3+"aaa3");
    }
    if((this.dash[this.a1]=="mall")){
      this.a4=this.a4+1;
      
    }

  };
    
      }
    this.lineDataJson = {
      'series':[
        'mois',
        ' taux participation',
      ],
      


      'data':[
        {

          'name': 'annuel',
          'value':[
            {
              'x':this.LL6[0],
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':20
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
            
            
          ]
        },
        {
          'name': 'maladiel',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':20
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
          
           
          ]
        },
        {
          'name': 'Rfamilial',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':20
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
            
           
          ]
        },
        {
          'name': 'patérnité',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':20
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
          
          
          ]
        },
        {
          'name': 'maladien',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':40*this.a3
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
           
           
          ]
        },
        {
          'name': 'matérnité',
          'value':[
            {
              'x':'oct',
              'y':15
            },
            {
              'x':'nov',
              'y':18
            },
            {
              'x':'déc',
              'y':10*this.a2
            },
            {
              'x':'janv',
              'y':33
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
           
          
          ]
        },
    	],
    };


    this.geoOrthographicDataJson =
    {
    'map':{
          'baseGeoDataUrl': 'https://raw.githubusercontent.com/Ohtsu/data/master/o2-chart/world.geojson',
          'keyDataName':'features',
          'targetPropertyName':'properties.name',
          'scale':160,
          'colorNumber':10,
          'rotate':{
            'horizontal':210,
            'vertical':5
          },
          'clipAngle':90,
          'oceanColor':'navy',
          'antarcticaColor':'white',
    	},
    	'data':[
        {
          'name':'Australia',
          'color':'red'
        },
        {
          'name':'Antarctica',
          'color':'white'
        },
        {
          'name':'Japan',
          'color':'teal'
        },
    	]
    }
	
    this.geoMapDataJson =
    {
    	'map':{
          'baseGeoDataUrl':'https://raw.githubusercontent.com/Ohtsu/data/master/o2-chart/world.geojson',
          'scale':75,
          'keyDataName':'features',
          'targetPropertyName':'properties.name',
    	},
    	'data':[
        {
          'name':'Australia',
          'color':'red'
        },
        {
          'name':'Antarctica',
          'color':'white'
        },
        {
          'name':'Japan',
          'color':'blue'
        },
    	],
    };

/*
    this.stackBarDataJson =
    {
    	'config':{
        'timeFormat':'%Y',
    	},
    	'series':[
        'mois',
        'absence',
    	],
    	'data':[
        {
          'name': 'annuel',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':20
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
            
           
          ]
        },
        {
          'name': 'maladiel',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':20
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
          
            
          ]
        },
        {
          'name': 'Rfamilial',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':20
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
           
           
          ]
        },
        {
          'name': 'patérnité',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':20
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
           
           
          ]
        },
        {
          'name': 'maladien',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':40*this.a3
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
           
          
          ]
        },
        {
          'name': 'matérnité',
          'value':[
            {
              'x':'oct',
              'y':15
            },
            {
              'x':'nov',
              'y':18
            },
            {
              'x':'déc',
              'y':10*this.a2
            },
            {
              'x':'janv',
              'y':33
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
            
           
          ]
        },
    	],
    };*/
  })


    this.scatterPlotDataJson =
    {
    	'series':[
        'seriesA',
        'seriesB',
        'seriesC'
    	],
    	'data':[
        {
          'name': 'suzuki',
          'value':[
            {'x':30,'y':40,'r':5},
            {'x':120,'y':115,'r':10},
            {'x':125,'y':90,'r':2},
            {'x':150,'y':160,'r':1},
            {'x':150,'y':160,'r':3},
            {'x':128,'y':215,'r':5},
            {'x':130,'y':40,'r':15},
            {'x':220,'y':115,'r':25},
          ]
        },
        {
          'name': 'inoue',
          'value':[
            {'x':130,'y':140,'r':5},
            {'x':20,'y':15,'r':10},
            {'x':25,'y':190,'r':2},
            {'x':250,'y':60,'r':1},
            {'x':50,'y':60,'r':3},
            {'x':28,'y':15,'r':5},
            {'x':230,'y':140,'r':15},
            {'x':20,'y':215,'r':25},
          ]
        },
    	],
    };

    this.histogramDataJson =
    {
    	'range':[0,100],
    	'bins': [0,10,20,30,40,50,60,70,80,90,100],
    	'data':[
        50,95,60,44,60,50,35,20,10,8,
        56,70,65,42,22,33,40,53,52,89,
        90,55,50,55,65,72,45,35,15,45,
        50,95,60,44,60,50,35,20,10,8,
        56,70,65,42,22,33,40,53,52,89,
        90,55,50,55,65,72,45,35,15,45,
        50,95,60,44,60,50,35,20,10,8,
        56,70,65,42,22,33,40,53,52,89,
        90,55,50,55,65,72,45,35,15,45,
    	],
    };


    this.packLayoutDataJson = {
    	'name':'United States', 'value' :281421906,
    	'children' : [
        {'name':'California', 'value' :33871648},
        {'name':'Texas', 'value' :20851820},
        {'name':'New York', 'value' :18976457},
        {'name':'Florida', 'value' :15982378},
        {'name':'Illinois', 'value' :12419293},
        {'name':'Pennsylvania', 'value' :12281054},
        {'name':'Ohio', 'value' :11353140},
        ]
    }

        this.treeDataJson =
        {
            'name': 'Eve',
            'children': [
                { 'name': 'Cain'
                },
                {
                    'name': 'Seth',
                    'children': [
                        { 'name': 'Enos' },
                        { 'name': 'Noam' }
                    ]
                },
                { 'name': 'Abel'
                },
                {
                    'name': 'Awan',
                    'children': [
                        { 'name': 'Enoch' }
                    ]
                },
                { 'name': 'Azura'
                },
            ]
        };


    this.treeMapDataJson = {
    	'name': 'Root',
    	'children': [
        { 'name': 'Dir1', 'children': [
            { 'name': 'Dir2', 'children': [
                { 'name': 'FileA', value: 5000 },
                { 'name': 'FileB', value: 3000 },
                { 'name': 'Dir3', 'children': [
                    { 'name': 'FileC', value: 2000 },
                    { 'name': 'Dir4', 'children': [
                        { 'name': 'FileD', value: 1000 },
                        { 'name': 'FileE', value: 1500 }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
    	]
    }


    this.choroplethDataJson = {
    	'map':{
        'baseGeoDataUrl':'https://raw.githubusercontent.com/Ohtsu/data/master/o2-chart/japan.geojson',
        'scale':900,
        'center':[137.571,37.500],
        'startColor':'blue',
        'endColor':'red',
        'colorNumber':10,
        'keyDataName':'features',
        'targetPropertyName':'properties.id'
    	},

    	'data':
    	[
        {
          'id':1,
          'value':7.12
        },
        {
          'id':2,
          'value':8.97
        },
        {
          'id':3,
          'value':7.07
        },
        {
          'id':4,
          'value':7.78
        },
        {
          'id':5,
          'value':6.97
        },
        {
          'id':6,
          'value':5.79
        },
        {
          'id':7,
          'value':7.14
        },
        {
          'id':8,
          'value':6.68
        },
        {
          'id':9,
          'value':6.28
        },
        {
          'id':10,
          'value':6.32
        },
        {
          'id':11,
          'value':6.29
        },
        {
          'id':12,
          'value':6.14
        },
        {
          'id':13,
          'value':5.87
        },
        {
          'id':14,
          'value':5.75
        },
        {
          'id':15,
          'value':5.50
        },
        {
          'id':16,
          'value':5.21
        },
        {
          'id':17,
          'value':5.37
        },
        {
          'id':18,
          'value':5.23
        },
        {
          'id':19,
          'value':6.18
        },
        {
          'id':20,
          'value':5.44
        },
        {
          'id':21,
          'value':5.57
        },
        {
          'id':22,
          'value':5.81
        },
        {
          'id':23,
          'value':5.09
        },
        {
          'id':24,
          'value':5.08
        },
        {
          'id':25,
          'value':5.07
        },
        {
          'id':26,
          'value':6.21
        },
        {
          'id':27,
          'value':7.97
        },
        {
          'id':28,
          'value':6.54
        },
        {
          'id':29,
          'value':7.41
        },
        {
          'id':30,
          'value':6.74
        },
        {
          'id':31,
          'value':5.90
        },
        {
          'id':32,
          'value':4.55
        },
        {
          'id':33,
          'value':7.24
        },
        {
          'id':34,
          'value':5.35
        },
        {
          'id':35,
          'value':5.93
        },
        {
          'id':36,
          'value':7.62
        },
        {
          'id':37,
          'value':6.25
        },
        {
          'id':38,
          'value':7.26
        },
        {
          'id':39,
          'value':7.70
        },
        {
          'id':40,
          'value':7.84
        },
        {
          'id':41,
          'value':6.32
        },
        {
          'id':42,
          'value':6.64
        },
        {
          'id':43,
          'value':6.67
        },
        {
          'id':44,
          'value':7.07
        },
        {
          'id':45,
          'value':7.01
        },
        {
          'id':46,
          'value':6.84
        },
        {
          'id':47,
          'value':11.0
        }
    	]
    };
    this.userService.getUsers().subscribe(data=>{this.listca=data;
      for (var i = 0; i < this.listca.length; i++) {
     this.array2=this.listca[i].absences;
      console.log(this.array);
            console.log(this.p18+"p1333333");
 
    
  }})

    this.forceDataJson =
    {
    	'groups': [
        {'id': 1, 'name': 'Hokkaido'},
        {'id': 2, 'name': 'Tohoku'},
        {'id': 3, 'name': 'Kanto'},
        {'id': 4, 'name': 'Chubu'},
        {'id': 5, 'name': 'kinki'},
        {'id': 6, 'name': 'Chugoku'},
        {'id': 7, 'name': 'Shikoku'},
        {'id': 8, 'name': 'Kyushu'},
    	],
    	'nodes': [
        {'id': 'Sapporo', 'group': 1},
        {'id': 'Sendai', 'group': 2},
        {'id': 'Morioka', 'group': 2},
        {'id': 'Akita', 'group': 2},
        {'id': 'Fukushima', 'group': 2},
        {'id': 'Mito', 'group': 3},
        {'id': 'Utsunomiya', 'group': 3},
        {'id': 'Saitama', 'group': 3},
        {'id': 'Chiba', 'group': 3},
        {'id': 'Tokyo', 'group': 3},
        {'id': 'Kofu', 'group': 4},
        {'id': 'Nagano', 'group': 4},
        {'id': 'Niigata', 'group': 4},
        {'id': 'Toyama', 'group': 4},
        {'id': 'Kanazawa', 'group': 4},
        {'id': 'Fukui', 'group': 4},
        {'id': 'Shizuoka', 'group': 4},
        {'id': 'Nagoya', 'group': 4},
        {'id': 'Gifu', 'group': 4},
        {'id': 'Otsu', 'group': 5},
        {'id': 'Kyoto', 'group': 5},
        {'id': 'Osaka', 'group': 5},
        {'id': 'Kobe', 'group': 5},
        {'id': 'Nara', 'group': 5},
        {'id': 'Kyoto', 'group': 5},
        {'id': 'Tottori', 'group': 6},
        {'id': 'Hiroshima', 'group': 6},
        {'id': 'Matsue', 'group': 6},
        {'id': 'Matsuyama', 'group': 7},
        {'id': 'Tokushima', 'group': 7},
        {'id': 'Kochi', 'group': 7},
        {'id': 'Fukuoka', 'group': 8},
        {'id': 'Nagasaki', 'group': 8},
        {'id': 'Kumamoto', 'group': 8},
        {'id': 'Naha', 'group': 8},
    	],
    	'links': [
            {'source': 'Sendai', 'target': 'Sapporo', 'value': 1},
            {'source': 'Morioka', 'target': 'Sapporo', 'value': 1},
            {'source': 'Akita', 'target': 'Sapporo', 'value': 1},
            {'source': 'Fukushima', 'target': 'Sapporo', 'value': 1},
            {'source': 'Morioka', 'target': 'Sendai', 'value': 10},
            {'source': 'Akita', 'target': 'Sendai', 'value': 10},
            {'source': 'Fukushima', 'target': 'Sendai', 'value': 10},
            {'source': 'Chiba', 'target': 'Tokyo', 'value': 20},
            {'source': 'Utsunomiya', 'target': 'Tokyo', 'value': 20},
            {'source': 'Mito', 'target': 'Tokyo', 'value': 20},
            {'source': 'Saitama', 'target': 'Tokyo', 'value': 30},
            {'source': 'Kofu', 'target': 'Tokyo', 'value': 30},
            {'source': 'Nagano', 'target': 'Tokyo', 'value': 30},
            {'source': 'Naha', 'target': 'Tokyo', 'value': 30},
            {'source': 'Osaka', 'target': 'Tokyo', 'value': 40},
            {'source': 'Sendai', 'target': 'Tokyo', 'value': 40},
            {'source': 'Hiroshima', 'target': 'Tokyo', 'value': 20},
            {'source': 'Shizuoka', 'target': 'Nagoya', 'value': 10},
            {'source': 'Tokyo', 'target': 'Nagoya', 'value': 40},
            {'source': 'Osaka', 'target': 'Nagoya', 'value': 40},
            {'source': 'Kyoto', 'target': 'Nagoya', 'value': 40},
            {'source': 'Kyoto', 'target': 'Osaka', 'value': 30},
            {'source': 'Hiroshima', 'target': 'Osaka', 'value': 20},
            {'source': 'Toyama', 'target': 'Kanazawa', 'value': 10},
            {'source': 'Fukui', 'target': 'Kanazawa', 'value': 10},
            {'source': 'Niigata', 'target': 'Kanazawa', 'value': 10},
            {'source': 'Tottori', 'target': 'Kobe', 'value': 10},
            {'source': 'Tottori', 'target': 'Hiroshima', 'value': 10},
            {'source': 'Matsue', 'target': 'Hiroshima', 'value': 10},
            {'source': 'Matsuyama', 'target': 'Hiroshima', 'value': 10},
            {'source': 'Tokushima', 'target': 'Kochi', 'value': 10},
            {'source': 'Matsuyama', 'target': 'Kochi', 'value': 10},
            {'source': 'Nagasaki', 'target': 'Fukuoka', 'value': 10},
            {'source': 'Kumamoto', 'target': 'Fukuoka', 'value': 10},
            {'source': 'Naha', 'target': 'Fukuoka', 'value': 10},
               ]
        };






  }

  private initilizeData() {
    this.userService.getUsersmd().subscribe(data=>{this.users=data; 
console.log(this.users+"patapatapata"); });

    this.userService.getUsers().subscribe(data=>{this.listca=data;
      for (var i = 0; i < this.listca.length; i++) {
        
     this.array2=this.listca[i].absences;
      console.log(this.array);
            
    }})
    //this.email=localStorage.getItem('token');

    ///this.email=this.userService.getter44();
    console.log(this.email)+"emaaaaaaaaaaaaaaaaaaaaaaiiiil";
    console.log(localStorage.getItem('token'));
    window.alert("okk");
    console.log(JSON.stringify(this.model));
    this.userService.createUserLog1().subscribe ( (ListPresence)=>{console.log(ListPresence);
      if(ListPresence){
      console.log(ListPresence);
      console.log(parseInt("12545"));
      console.log(ListPresence.length);
      this.nb1=0;
      this.nb=0;
      this.nb2=0;
      this.nb3=0;
      this.nb4=0;
      this.nb5=0;
      this.n6=0;
      this.n7=0;
      this.n8=0;
      this.n9=0;
      this.n10=0;
      this.n11=0;
      this.n12=0;
      this.n13=0;
      this.n14=0;
      this.n15=0;
      this.n16=0;
      this.n17=0;
      this.n18=0;
      this.n19=0;
      
      for(this.nb ; this.nb<ListPresence.length; this.nb++){
        if((ListPresence[this.nb]=="12")){
console.log("yeeeeeeeeeees");
this.nb1=this.nb1+1;
console.log(this.nb1);
        };    
        if((ListPresence[this.nb]=="11")){
          console.log("yeeeeeeeeeees");
          this.nb3=this.nb3+1;
          console.log(this.nb3);
                  };      
                  if((ListPresence[this.nb]=="10")){
                    console.log("yeeeeeeeeeees");
                    this.nb4=this.nb4+1;
                    console.log(this.nb4);
                            };    
                            if((ListPresence[this.nb]=="9")){
                              console.log("yeeeeeeeeeees");
                              this.nb5=this.nb5+1;
                              console.log(this.nb5);
                                      };    
                                      if((ListPresence[this.nb]=="1")){
                                        console.log("yeeeeeeeeeees");
                                        this.n6=this.n6+1;
                                        console.log(this.n6);
                                                };    
                                                if((ListPresence[this.nb]=="2")){
                                                  console.log("yeeeeeeeeeees");
                                                  this.n7=this.n7+1;
                                                  console.log(this.n7);
                                                          };            if((ListPresence[this.nb]=="3")){
                                                            console.log("yeeeeeeeeeees");
                                                            this.n8=this.n8+1;
                                                            console.log(this.n8);
                                                                    };           
                                                                    if((ListPresence[this.nb]=="4")){
                                                                      console.log("yeeeeeeeeeees");
                                                                      this.n9=this.n9+1;
                                                                      console.log(this.n9);
                                                                              };           
                                                                              if((ListPresence[this.nb]=="5")){
                                                                                console.log("yeeeeeeeeeees");
                                                                                this.n10=this.n10+1;
                                                                                console.log(this.n10);
                                                                                        };   
                                                                                        
      }this.nb2=this.nb1;
      this.n11=this.nb3;
      this.n12=this.nb4;
      this.n13=this.nb5;
      this.n14=this.n6;
      this.n15=this.n7;
      this.n16=this.n8;
      this.n17=this.n9;
      this.n18=this.n10;
/*
      console.log(this.nb2+"ytytyyyyyyyyyyyyyyy");
      this.barDataJson =
      {
        'series':[
          'Presence',
          'Absence'
        ],
        'data':[
          {
            'x': 'sep',
            'y': [10*this.n18,100-10*this.n18],
          },
          {
            'x': 'oct',
            'y': [10*this.n12,100-10*this.n12],
          },
          {
            'x': 'nov',
            'y': [10*this.n11,100-10*this.n11],
          },
          {
            'x': 'dec',
            'y': [10*this.nb2,100-10*this.nb2],
          },
          {
            'x': 'janv',
            'y': [10*this.n14,100-10*this.n14],
          },
          {
            'x': 'fev',
            'y': [10*this.n15,100-10*this.n15],
          },
          {
            'x': 'mars',
            'y': [10*this.n16,100-10*this.n16],
          },
          {
            'x': 'avril',
            'y': [10*this.n17,100-10*this.n17],
          },
          {
            'x': 'mai',
            'y': [10*this.n18,100-10*this.n18],
          },
          {
            'x': 'juin',
            'y': [10*this.n18,100-10*this.n18],
          },
          {
            'x': 'juillet',
            'y': [10*this.n18,100-10*this.n18],
          },
          {
            'x': 'aout',
            'y': [10*this.n18,100-10*this.n18],
          },
        ],
      };*/
      
      
    
     } 
    
    })




    // ConfigData = this.httpClient.get('assets/json/ConfigData.json');
    this.configData = {
      // tslint:disable-next-line:quotemark
      "className": {
        'axis': 'axis',
        'axisXBorder': 'axis_x',
        'axisXText': 'axis-x-text',
        'bar': 'bar',
        'barValue': 'bar-value',
        'line': 'line',
        'multiLinePrefix': 'line-',
        'grid': 'grid',
        'pie': 'pie',
        'pieInnerTitle': 'pie-inner-title',
        'pieInnerRadius': 'total',
        'histogram': 'histogram',
        'histogramBar': 'histogram-bar',
        'treemap': 'treemap',
        'treemapLabel': 'treemap-label',
        'packlayout': 'packlayout',
        'packlayoutLabel': 'packlayout-label',
      },
      'label': {
          'display': true,
      },
      'title': {
        'display': true,
        'name': 'Title',
        'className': 'chart-title',
        'height': 30,
        'leftMargin': -20,
        'bottomMargin': 10
      },
      'maxValue': {
        'auto': true,
        'x': 100,
        'y': 100,
      },
      'legend': {
        'display': true,
        'position':           'right',
        'totalWidth': 80,
        'initXPos': 5,
        'initYPos': 10,
        'rectWidth': 10,
        'rectHeight': 10,
        'xSpacing': 2,
        'ySpacing': 2
      },
      'color': {
        'auto': true, //
        'defaultColorNumber': 10,
        'opacity': 1.0,
        'userColors': [
          'blue',
          'red',
          'green',
          'yellow',
          'PaleGoldenrod',
          'Khaki',
          'DarkKhaki',
          'Gold',
          'Cornsilk',
          'BlanchedAlmond',
          'Bisque',
          'NavajoWhite',
          'Wheat',
          'BurlyWood',
          'Tan',
          'RosyBrown',
          'SandyBrown',
          'Goldenrod',
          'DarkGoldenrod',
          'Peru',
          'Chocolate'
        ],
        'focusColor': 'red',
      },
      'pie': {
        'innerRadius': {
          'percent': 20,
          'title': 'Total'
        },
        'value': {
          'display': true,
        },
        'percent':{
          'display': false,
        }
      },
      'line': {
        'legend': 'lineEnd',
        'interpolate' : 'linear',
      },
      'grid': {
        'x': {
          'display': true,
        },
        'y':{
          'display': true,
        },
      },
      'margin': {
        'top': 30,
        'left': 30,
        'right': 10,
        'bottom': 20,
        'between': 5
      },
      'axis': {
        'rotation': 0,
        'borderLineWidth': 1,
        'xLabel': {
          'leftMargin': 0,
          'bottomMargin': 5
        },
        'yLabel':{
          'leftMargin': 0,
          'bottomMargin': 0
        },
      },
      'animation':{
        'enable':true,
        'duration':4000,
      },
    };
    this.userService.getUsers().subscribe(data=>{this.listca=data;
      for (var i = 0; i < this.listca.length; i++) {
     this.array2=this.listca[i].absences;
      console.log(this.array);
            
    }})
    ///this.email=localStorage.getItem('token');
    console.log(localStorage.getItem('token'));
    this.userService.createUserLog1().subscribe ( (ListPresence3)=>{console.log(ListPresence3+"liiist3333");
    this.ListPresence4=ListPresence3;})
    this.userService.createUserLog2dash(this.email).subscribe ( (ListPresencedash)=>{console.log(ListPresencedash+"noooooooooo1");

this.dash=ListPresencedash;
this.a1=0;
this.a2=0;
this.a3=0;
this.a4=0;
this.a5=0;
this.a6=0;
this.a7=0;
this.a8=0;
this.a9=0;
this.a10=0;
this.a11=0;
this.a12=0;
this.a13=0;
this.a14=0;
this.a15=0;
this.a16=0;
this.a17=0;
this.a18=0;
this.a19=0;
this.b1=null;
this.b2=null;
for(this.a1 ; this.a1<this.ListPresence4.length; this.a1++){
  console.log(this.ListPresence4.length);
  console.log(this.dash.length);
  if((this.ListPresence4[this.a1]=="12")){
    console.log("matttttppppp");
    if((this.dash[this.a1]=="mat")){
      console.log("mattttt");
      this.a2=this.a2+1;
      console.log("mattttt2222222227777777");
      console.log(this.a2);

    }
    if((this.dash[this.a1]=="maln")){
      this.a3=this.a3+1;
      console.log(this.a3+"aaa3");
    }
    if((this.dash[this.a1]=="mall")){
      this.a4=this.a4+1;
      
    }

  };
    
      }
    this.lineDataJson = {
      'series':[
        'mois',
        'absence',
      ],
      


      'data':[
        {

          'name': 'annuel',
          'value':[
            {
              'x':this.LL6[0],
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':20
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
            
            
          ]
        },
        {
          'name': 'maladiel',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':20
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
          
           
          ]
        },
        {
          'name': 'Rfamilial',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':20
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
            
           
          ]
        },
        {
          'name': 'patérnité',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':20
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
          
          
          ]
        },
        {
          'name': 'maladien',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':40*this.a3
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
           
           
          ]
        },
        {
          'name': 'matérnité',
          'value':[
            {
              'x':'oct',
              'y':15
            },
            {
              'x':'nov',
              'y':18
            },
            {
              'x':'déc',
              'y':10*this.a2
            },
            {
              'x':'janv',
              'y':33
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
           
          
          ]
        },
    	],
    };


    this.geoOrthographicDataJson =
    {
    'map':{
          'baseGeoDataUrl': 'https://raw.githubusercontent.com/Ohtsu/data/master/o2-chart/world.geojson',
          'keyDataName':'features',
          'targetPropertyName':'properties.name',
          'scale':160,
          'colorNumber':10,
          'rotate':{
            'horizontal':210,
            'vertical':5
          },
          'clipAngle':90,
          'oceanColor':'navy',
          'antarcticaColor':'white',
    	},
    	'data':[
        {
          'name':'Australia',
          'color':'red'
        },
        {
          'name':'Antarctica',
          'color':'white'
        },
        {
          'name':'Japan',
          'color':'teal'
        },
    	]
    }
	
    this.geoMapDataJson =
    {
    	'map':{
          'baseGeoDataUrl':'https://raw.githubusercontent.com/Ohtsu/data/master/o2-chart/world.geojson',
          'scale':75,
          'keyDataName':'features',
          'targetPropertyName':'properties.name',
    	},
    	'data':[
        {
          'name':'Australia',
          'color':'red'
        },
        {
          'name':'Antarctica',
          'color':'white'
        },
        {
          'name':'Japan',
          'color':'blue'
        },
    	],
    };

/*
    this.stackBarDataJson =
    {
    	'config':{
        'timeFormat':'%Y',
    	},
    	'series':[
        'mois',
        'absence',
    	],
    	'data':[
        {
          'name': 'annuel',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':20
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
            
           
          ]
        },
        {
          'name': 'maladiel',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':20
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
          
            
          ]
        },
        {
          'name': 'Rfamilial',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':20
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
           
           
          ]
        },
        {
          'name': 'patérnité',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':20
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
           
           
          ]
        },
        {
          'name': 'maladien',
          'value':[
            {
              'x':'oct',
              'y':5
            },
            {
              'x':'nov',
              'y':16
            },
            {
              'x':'déc',
              'y':40*this.a3
            },
            {
              'x':'janv',
              'y':41
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
           
          
          ]
        },
        {
          'name': 'matérnité',
          'value':[
            {
              'x':'oct',
              'y':15
            },
            {
              'x':'nov',
              'y':18
            },
            {
              'x':'déc',
              'y':10*this.a2
            },
            {
              'x':'janv',
              'y':33
            },
            {
              'x':'fév',
              'y':41
            },
            {
              'x':'mars',
              'y':41
            },
            
           
          ]
        },
    	],
    };*/
  })


    this.scatterPlotDataJson =
    {
    	'series':[
        'seriesA',
        'seriesB',
        'seriesC'
    	],
    	'data':[
        {
          'name': 'suzuki',
          'value':[
            {'x':30,'y':40,'r':5},
            {'x':120,'y':115,'r':10},
            {'x':125,'y':90,'r':2},
            {'x':150,'y':160,'r':1},
            {'x':150,'y':160,'r':3},
            {'x':128,'y':215,'r':5},
            {'x':130,'y':40,'r':15},
            {'x':220,'y':115,'r':25},
          ]
        },
        {
          'name': 'inoue',
          'value':[
            {'x':130,'y':140,'r':5},
            {'x':20,'y':15,'r':10},
            {'x':25,'y':190,'r':2},
            {'x':250,'y':60,'r':1},
            {'x':50,'y':60,'r':3},
            {'x':28,'y':15,'r':5},
            {'x':230,'y':140,'r':15},
            {'x':20,'y':215,'r':25},
          ]
        },
    	],
    };

    this.histogramDataJson =
    {
    	'range':[0,100],
    	'bins': [0,10,20,30,40,50,60,70,80,90,100],
    	'data':[
        50,95,60,44,60,50,35,20,10,8,
        56,70,65,42,22,33,40,53,52,89,
        90,55,50,55,65,72,45,35,15,45,
        50,95,60,44,60,50,35,20,10,8,
        56,70,65,42,22,33,40,53,52,89,
        90,55,50,55,65,72,45,35,15,45,
        50,95,60,44,60,50,35,20,10,8,
        56,70,65,42,22,33,40,53,52,89,
        90,55,50,55,65,72,45,35,15,45,
    	],
    };


    this.packLayoutDataJson = {
    	'name':'United States', 'value' :281421906,
    	'children' : [
        {'name':'California', 'value' :33871648},
        {'name':'Texas', 'value' :20851820},
        {'name':'New York', 'value' :18976457},
        {'name':'Florida', 'value' :15982378},
        {'name':'Illinois', 'value' :12419293},
        {'name':'Pennsylvania', 'value' :12281054},
        {'name':'Ohio', 'value' :11353140},
        ]
    }

        this.treeDataJson =
        {
            'name': 'Eve',
            'children': [
                { 'name': 'Cain'
                },
                {
                    'name': 'Seth',
                    'children': [
                        { 'name': 'Enos' },
                        { 'name': 'Noam' }
                    ]
                },
                { 'name': 'Abel'
                },
                {
                    'name': 'Awan',
                    'children': [
                        { 'name': 'Enoch' }
                    ]
                },
                { 'name': 'Azura'
                },
            ]
        };


    this.treeMapDataJson = {
    	'name': 'Root',
    	'children': [
        { 'name': 'Dir1', 'children': [
            { 'name': 'Dir2', 'children': [
                { 'name': 'FileA', value: 5000 },
                { 'name': 'FileB', value: 3000 },
                { 'name': 'Dir3', 'children': [
                    { 'name': 'FileC', value: 2000 },
                    { 'name': 'Dir4', 'children': [
                        { 'name': 'FileD', value: 1000 },
                        { 'name': 'FileE', value: 1500 }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
    	]
    }


    this.choroplethDataJson = {
    	'map':{
        'baseGeoDataUrl':'https://raw.githubusercontent.com/Ohtsu/data/master/o2-chart/japan.geojson',
        'scale':900,
        'center':[137.571,37.500],
        'startColor':'blue',
        'endColor':'red',
        'colorNumber':10,
        'keyDataName':'features',
        'targetPropertyName':'properties.id'
    	},

    	'data':
    	[
        {
          'id':1,
          'value':7.12
        },
        {
          'id':2,
          'value':8.97
        },
        {
          'id':3,
          'value':7.07
        },
        {
          'id':4,
          'value':7.78
        },
        {
          'id':5,
          'value':6.97
        },
        {
          'id':6,
          'value':5.79
        },
        {
          'id':7,
          'value':7.14
        },
        {
          'id':8,
          'value':6.68
        },
        {
          'id':9,
          'value':6.28
        },
        {
          'id':10,
          'value':6.32
        },
        {
          'id':11,
          'value':6.29
        },
        {
          'id':12,
          'value':6.14
        },
        {
          'id':13,
          'value':5.87
        },
        {
          'id':14,
          'value':5.75
        },
        {
          'id':15,
          'value':5.50
        },
        {
          'id':16,
          'value':5.21
        },
        {
          'id':17,
          'value':5.37
        },
        {
          'id':18,
          'value':5.23
        },
        {
          'id':19,
          'value':6.18
        },
        {
          'id':20,
          'value':5.44
        },
        {
          'id':21,
          'value':5.57
        },
        {
          'id':22,
          'value':5.81
        },
        {
          'id':23,
          'value':5.09
        },
        {
          'id':24,
          'value':5.08
        },
        {
          'id':25,
          'value':5.07
        },
        {
          'id':26,
          'value':6.21
        },
        {
          'id':27,
          'value':7.97
        },
        {
          'id':28,
          'value':6.54
        },
        {
          'id':29,
          'value':7.41
        },
        {
          'id':30,
          'value':6.74
        },
        {
          'id':31,
          'value':5.90
        },
        {
          'id':32,
          'value':4.55
        },
        {
          'id':33,
          'value':7.24
        },
        {
          'id':34,
          'value':5.35
        },
        {
          'id':35,
          'value':5.93
        },
        {
          'id':36,
          'value':7.62
        },
        {
          'id':37,
          'value':6.25
        },
        {
          'id':38,
          'value':7.26
        },
        {
          'id':39,
          'value':7.70
        },
        {
          'id':40,
          'value':7.84
        },
        {
          'id':41,
          'value':6.32
        },
        {
          'id':42,
          'value':6.64
        },
        {
          'id':43,
          'value':6.67
        },
        {
          'id':44,
          'value':7.07
        },
        {
          'id':45,
          'value':7.01
        },
        {
          'id':46,
          'value':6.84
        },
        {
          'id':47,
          'value':11.0
        }
    	]
    };
    this.userService.getUsers().subscribe(data=>{this.listca=data;
      for (var i = 0; i < this.listca.length; i++) {
     this.array2=this.listca[i].absences;
      console.log(this.array);
            console.log(this.p18+"p1333333");
 
    
  }})

/*
today
  this.barDataJson =
  {
    'series':[
      'English',
      'Math'
    ],
    'data':[
      {
        'x': 'suzuki',
        'y': [92,73],
      },
      {
        'x': 'inoue',
        'y': [69,45],
      },
      {
        'x': 'sato',
        'y': [70,100],
      },
      {
        'x': 'tanaka',
        'y': [43,66],
      },
      {
        'x': 'ida',
        'y': [60,70],
      },
      {
        'x': 'kato',
        'y': [55,63],
      },
    ],
  };

  /* todaythis.lineDataJson = {
    'series':[
      'year',
      'sell',
    ],
    'data':[
      {
        'name': 'software',
        'value':[
          {
            'x':'2010',
            'y':18
          },
          {
            'x':'2011',
            'y':22
          },
          {
            'x':'2012',
            'y':30
          },
          {
            'x':'2013',
            'y':31
          },
        ]
      },
      {
        'name': 'hardware',
        'value':[
          {
            'x':'2010',
            'y':15
          },
          {
            'x':'2011',
            'y':16
          },
          {
            'x':'2012',
            'y':10
          },
          {
            'x':'2013',
            'y':21
          },
        ]
      },
      {
        'name': 'device',
        'value':[
          {
            'x':'2010',
            'y':25
          },
          {
            'x':'2011',
            'y':26
          },
          {
            'x':'2012',
            'y':30
          },
          {
            'x':'2013',
            'y':31
          },
        ]
      },
      {
        'name': 'others',
        'value':[
          {
            'x':'2010',
            'y':100
          },
          {
            'x':'2011',
            'y':16
          },
          {
            'x':'2012',
            'y':20
          },
          {
            'x':'2013',
            'y':41
          },
        ]
      },
    ],
  };*/


  this.geoOrthographicDataJson =
  {
  'map':{
        'baseGeoDataUrl': 'https://raw.githubusercontent.com/Ohtsu/data/master/o2-chart/world.geojson',
        'keyDataName':'features',
        'targetPropertyName':'properties.name',
        'scale':160,
        'colorNumber':10,
        'rotate':{
          'horizontal':210,
          'vertical':5
        },
        'clipAngle':90,
        'oceanColor':'navy',
        'antarcticaColor':'white',
    },
    'data':[
      {
        'name':'Australia',
        'color':'red'
      },
      {
        'name':'Antarctica',
        'color':'white'
      },
      {
        'name':'Japan',
        'color':'teal'
      },
    ]
  }

  this.geoMapDataJson =
  {
    'map':{
        'baseGeoDataUrl':'https://raw.githubusercontent.com/Ohtsu/data/master/o2-chart/world.geojson',
        'scale':75,
        'keyDataName':'features',
        'targetPropertyName':'properties.name',
    },
    'data':[
      {
        'name':'Australia',
        'color':'red'
      },
      {
        'name':'Antarctica',
        'color':'white'
      },
      {
        'name':'Japan',
        'color':'blue'
      },
    ],
  };


  this.stackBarDataJson =
  {
    'config':{
      'timeFormat':'%Y',
    },
    'series':[
      'year',
      'sell',
    ],
    'data':[
      {
        'name': 'software',
        'value':[
          {
            'x':'2010',
            'y':18
          },
          {
            'x':'2011',
            'y':22
          },
          {
            'x':'2012',
            'y':30
          },
          {
            'x':'2013',
            'y':31
          },
        ]
      },
      {
        'name': 'hardware',
        'value':[
          {
            'x':'2010',
            'y':15
          },
          {
            'x':'2011',
            'y':16
          },
          {
            'x':'2012',
            'y':10
          },
          {
            'x':'2013',
            'y':21
          },
        ]
      },
      {
        'name': 'device',
        'value':[
          {
            'x':'2010',
            'y':25
          },
          {
            'x':'2011',
            'y':26
          },
          {
            'x':'2012',
            'y':30
          },
          {
            'x':'2013',
            'y':31
          },
        ]
      },
      {
        'name': 'others',
        'value':[
          {
            'x':'2010',
            'y':5
          },
          {
            'x':'2011',
            'y':16
          },
          {
            'x':'2012',
            'y':20
          },
          {
            'x':'2013',
            'y':41
          },
        ]
      },
    ],
  };



  this.scatterPlotDataJson =
  {
    'series':[
      'seriesA',
      'seriesB',
      'seriesC'
    ],
    'data':[
      {
        'name': 'suzuki',
        'value':[
          {'x':30,'y':40,'r':5},
          {'x':120,'y':115,'r':10},
          {'x':125,'y':90,'r':2},
          {'x':150,'y':160,'r':1},
          {'x':150,'y':160,'r':3},
          {'x':128,'y':215,'r':5},
          {'x':130,'y':40,'r':15},
          {'x':220,'y':115,'r':25},
        ]
      },
      {
        'name': 'inoue',
        'value':[
          {'x':130,'y':140,'r':5},
          {'x':20,'y':15,'r':10},
          {'x':25,'y':190,'r':2},
          {'x':250,'y':60,'r':1},
          {'x':50,'y':60,'r':3},
          {'x':28,'y':15,'r':5},
          {'x':230,'y':140,'r':15},
          {'x':20,'y':215,'r':25},
        ]
      },
    ],
  };

  this.histogramDataJson =
  {
    'range':[0,100],
    'bins': [0,10,20,30,40,50,60,70,80,90,100],
    'data':[
      50,95,60,44,60,50,35,20,10,8,
      56,70,65,42,22,33,40,53,52,89,
      90,55,50,55,65,72,45,35,15,45,
      50,95,60,44,60,50,35,20,10,8,
      56,70,65,42,22,33,40,53,52,89,
      90,55,50,55,65,72,45,35,15,45,
      50,95,60,44,60,50,35,20,10,8,
      56,70,65,42,22,33,40,53,52,89,
      90,55,50,55,65,72,45,35,15,45,
    ],
  };


  this.packLayoutDataJson = {
    'name':'United States', 'value' :281421906,
    'children' : [
      {'name':'California', 'value' :33871648},
      {'name':'Texas', 'value' :20851820},
      {'name':'New York', 'value' :18976457},
      {'name':'Florida', 'value' :15982378},
      {'name':'Illinois', 'value' :12419293},
      {'name':'Pennsylvania', 'value' :12281054},
      {'name':'Ohio', 'value' :11353140},
      ]
  }

      this.treeDataJson =
      {
          'name': 'Eve',
          'children': [
              { 'name': 'Cain'
              },
              {
                  'name': 'Seth',
                  'children': [
                      { 'name': 'Enos' },
                      { 'name': 'Noam' }
                  ]
              },
              { 'name': 'Abel'
              },
              {
                  'name': 'Awan',
                  'children': [
                      { 'name': 'Enoch' }
                  ]
              },
              { 'name': 'Azura'
              },
          ]
      };


  this.treeMapDataJson = {
    'name': 'Root',
    'children': [
      { 'name': 'Dir1', 'children': [
          { 'name': 'Dir2', 'children': [
              { 'name': 'FileA', value: 5000 },
              { 'name': 'FileB', value: 3000 },
              { 'name': 'Dir3', 'children': [
                  { 'name': 'FileC', value: 2000 },
                  { 'name': 'Dir4', 'children': [
                      { 'name': 'FileD', value: 1000 },
                      { 'name': 'FileE', value: 1500 }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }


  this.choroplethDataJson = {
    'map':{
      'baseGeoDataUrl':'https://raw.githubusercontent.com/Ohtsu/data/master/o2-chart/japan.geojson',
      'scale':900,
      'center':[137.571,37.500],
      'startColor':'blue',
      'endColor':'red',
      'colorNumber':10,
      'keyDataName':'features',
      'targetPropertyName':'properties.id'
    },

    'data':
    [
      {
        'id':1,
        'value':7.12
      },
      {
        'id':2,
        'value':8.97
      },
      {
        'id':3,
        'value':7.07
      },
      {
        'id':4,
        'value':7.78
      },
      {
        'id':5,
        'value':6.97
      },
      {
        'id':6,
        'value':5.79
      },
      {
        'id':7,
        'value':7.14
      },
      {
        'id':8,
        'value':6.68
      },
      {
        'id':9,
        'value':6.28
      },
      {
        'id':10,
        'value':6.32
      },
      {
        'id':11,
        'value':6.29
      },
      {
        'id':12,
        'value':6.14
      },
      {
        'id':13,
        'value':5.87
      },
      {
        'id':14,
        'value':5.75
      },
      {
        'id':15,
        'value':5.50
      },
      {
        'id':16,
        'value':5.21
      },
      {
        'id':17,
        'value':5.37
      },
      {
        'id':18,
        'value':5.23
      },
      {
        'id':19,
        'value':6.18
      },
      {
        'id':20,
        'value':5.44
      },
      {
        'id':21,
        'value':5.57
      },
      {
        'id':22,
        'value':5.81
      },
      {
        'id':23,
        'value':5.09
      },
      {
        'id':24,
        'value':5.08
      },
      {
        'id':25,
        'value':5.07
      },
      {
        'id':26,
        'value':6.21
      },
      {
        'id':27,
        'value':7.97
      },
      {
        'id':28,
        'value':6.54
      },
      {
        'id':29,
        'value':7.41
      },
      {
        'id':30,
        'value':6.74
      },
      {
        'id':31,
        'value':5.90
      },
      {
        'id':32,
        'value':4.55
      },
      {
        'id':33,
        'value':7.24
      },
      {
        'id':34,
        'value':5.35
      },
      {
        'id':35,
        'value':5.93
      },
      {
        'id':36,
        'value':7.62
      },
      {
        'id':37,
        'value':6.25
      },
      {
        'id':38,
        'value':7.26
      },
      {
        'id':39,
        'value':7.70
      },
      {
        'id':40,
        'value':7.84
      },
      {
        'id':41,
        'value':6.32
      },
      {
        'id':42,
        'value':6.64
      },
      {
        'id':43,
        'value':6.67
      },
      {
        'id':44,
        'value':7.07
      },
      {
        'id':45,
        'value':7.01
      },
      {
        'id':46,
        'value':6.84
      },
      {
        'id':47,
        'value':11.0
      }
    ]
  };
  /* today

  this.pieDataJson =
  {
    'data':[
      {
        'name': 'software',
        'value':30,
      },
      {
        'name': 'hardware',
        'value':25
      },
      {
        'name': 'device',
        'value':16
      },
      {
        'name': 'others',
        'value':4
      },
    ],
  };
*/














    this.forceDataJson =
    {
    	'groups': [
        {'id': 1, 'name': 'Hokkaido'},
        {'id': 2, 'name': 'Tohoku'},
        {'id': 3, 'name': 'Kanto'},
        {'id': 4, 'name': 'Chubu'},
        {'id': 5, 'name': 'kinki'},
        {'id': 6, 'name': 'Chugoku'},
        {'id': 7, 'name': 'Shikoku'},
        {'id': 8, 'name': 'Kyushu'},
    	],
    	'nodes': [
        {'id': 'Sapporo', 'group': 1},
        {'id': 'Sendai', 'group': 2},
        {'id': 'Morioka', 'group': 2},
        {'id': 'Akita', 'group': 2},
        {'id': 'Fukushima', 'group': 2},
        {'id': 'Mito', 'group': 3},
        {'id': 'Utsunomiya', 'group': 3},
        {'id': 'Saitama', 'group': 3},
        {'id': 'Chiba', 'group': 3},
        {'id': 'Tokyo', 'group': 3},
        {'id': 'Kofu', 'group': 4},
        {'id': 'Nagano', 'group': 4},
        {'id': 'Niigata', 'group': 4},
        {'id': 'Toyama', 'group': 4},
        {'id': 'Kanazawa', 'group': 4},
        {'id': 'Fukui', 'group': 4},
        {'id': 'Shizuoka', 'group': 4},
        {'id': 'Nagoya', 'group': 4},
        {'id': 'Gifu', 'group': 4},
        {'id': 'Otsu', 'group': 5},
        {'id': 'Kyoto', 'group': 5},
        {'id': 'Osaka', 'group': 5},
        {'id': 'Kobe', 'group': 5},
        {'id': 'Nara', 'group': 5},
        {'id': 'Kyoto', 'group': 5},
        {'id': 'Tottori', 'group': 6},
        {'id': 'Hiroshima', 'group': 6},
        {'id': 'Matsue', 'group': 6},
        {'id': 'Matsuyama', 'group': 7},
        {'id': 'Tokushima', 'group': 7},
        {'id': 'Kochi', 'group': 7},
        {'id': 'Fukuoka', 'group': 8},
        {'id': 'Nagasaki', 'group': 8},
        {'id': 'Kumamoto', 'group': 8},
        {'id': 'Naha', 'group': 8},
    	],
    	'links': [
            {'source': 'Sendai', 'target': 'Sapporo', 'value': 1},
            {'source': 'Morioka', 'target': 'Sapporo', 'value': 1},
            {'source': 'Akita', 'target': 'Sapporo', 'value': 1},
            {'source': 'Fukushima', 'target': 'Sapporo', 'value': 1},
            {'source': 'Morioka', 'target': 'Sendai', 'value': 10},
            {'source': 'Akita', 'target': 'Sendai', 'value': 10},
            {'source': 'Fukushima', 'target': 'Sendai', 'value': 10},
            {'source': 'Chiba', 'target': 'Tokyo', 'value': 20},
            {'source': 'Utsunomiya', 'target': 'Tokyo', 'value': 20},
            {'source': 'Mito', 'target': 'Tokyo', 'value': 20},
            {'source': 'Saitama', 'target': 'Tokyo', 'value': 30},
            {'source': 'Kofu', 'target': 'Tokyo', 'value': 30},
            {'source': 'Nagano', 'target': 'Tokyo', 'value': 30},
            {'source': 'Naha', 'target': 'Tokyo', 'value': 30},
            {'source': 'Osaka', 'target': 'Tokyo', 'value': 40},
            {'source': 'Sendai', 'target': 'Tokyo', 'value': 40},
            {'source': 'Hiroshima', 'target': 'Tokyo', 'value': 20},
            {'source': 'Shizuoka', 'target': 'Nagoya', 'value': 10},
            {'source': 'Tokyo', 'target': 'Nagoya', 'value': 40},
            {'source': 'Osaka', 'target': 'Nagoya', 'value': 40},
            {'source': 'Kyoto', 'target': 'Nagoya', 'value': 40},
            {'source': 'Kyoto', 'target': 'Osaka', 'value': 30},
            {'source': 'Hiroshima', 'target': 'Osaka', 'value': 20},
            {'source': 'Toyama', 'target': 'Kanazawa', 'value': 10},
            {'source': 'Fukui', 'target': 'Kanazawa', 'value': 10},
            {'source': 'Niigata', 'target': 'Kanazawa', 'value': 10},
            {'source': 'Tottori', 'target': 'Kobe', 'value': 10},
            {'source': 'Tottori', 'target': 'Hiroshima', 'value': 10},
            {'source': 'Matsue', 'target': 'Hiroshima', 'value': 10},
            {'source': 'Matsuyama', 'target': 'Hiroshima', 'value': 10},
            {'source': 'Tokushima', 'target': 'Kochi', 'value': 10},
            {'source': 'Matsuyama', 'target': 'Kochi', 'value': 10},
            {'source': 'Nagasaki', 'target': 'Fukuoka', 'value': 10},
            {'source': 'Kumamoto', 'target': 'Fukuoka', 'value': 10},
            {'source': 'Naha', 'target': 'Fukuoka', 'value': 10},
               ]
        };


      }
  
}
