import { Time } from "@angular/common";
import { User } from "../user";

export class Demande {
    id: number;
    
    dated: Date;
    datef: Date;
    dater:Date;
    piece:string;
    avis:string;
    datedemande: Date;
    dateretoureffectif:Date;
    commentaire:String;
     h1:string;
     h2:string;
     urgence:string;
    nbjour:number;
    motif:string;
    rfa:number;
    confa:number;
    confu:number;
    refa:number;
    type:string;
    u:number;
    a:number;
  
}