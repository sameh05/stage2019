export class User {
    id: number;
   
    firstname: string;
    lastname: string;
    email: string;
    password: string;
    job:string;
    phone:number;
    autorisation :string;
    datenaissance : Date;
}