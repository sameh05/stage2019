import { Injectable } from '@angular/core';
import {Http,Response,Headers, RequestOptions} from '@angular/Http';
import {HttpClientModule} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map, count} from "rxjs/operators";
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {User} from '../user';

import {User2} from '../User2';

import { HttpClient } from '@angular/common/http';
import { Voiture } from '../voiture';
import { Demande } from '../apply/demande';
import { Message } from '../model';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  private baseUrl:string='http://localhost:8096/api';
  private user:User;
  private voiture:Voiture;
  private voitureo:Voiture;
  private demande1 :Demande;
  private user2:User2;
  private s:string;
  content: string;
  style: string;
  dismissed: boolean = false;
  user1=new User();
  demande=new Demande();
  id1:any;
  email:any;
 
  constructor(private http:HttpClient) { }

  d(s:string){
    return this.http.post(this.baseUrl+'/ss',s);
  }
  getUsers():Observable<any>{
    return this.http.get(this.baseUrl+'/users');

    
  
    ;}

        
  count():Observable<any>{

    return this.http.get(this.baseUrl+'/count' );
  }
  count1(id):Observable<any>{
return this.http.post(this.baseUrl+'/count1', id);


  }

  count3(id):Observable<any>{
    return this.http.post(this.baseUrl+'/count3', id);
    
    
      }
    getUsersca(id:String):Observable<any>{


      return this.http.post(this.baseUrl+'/usersca',id);

    }
    getU(user2Id:Number):Observable<any>{
      console.log(user2Id);
      console.log( this.http.get(this.baseUrl+'/'+user2Id));

      return this.http.get(this.baseUrl+'/'+user2Id);
      
    
    
      }
      U(voituremarque:String):Observable<any>{
        console.log(voituremarque);
        console.log( this.http.get(this.baseUrl+'/v'+voituremarque));
  
      return this.http.get(this.baseUrl+'/v'+voituremarque);
        
      
      
        }
        Uv1():Observable<any>{

          return this.http.get(this.baseUrl+'/users');
        }


        Uv(id:number):Observable<any>{
          console.log(id);
          console.log( this.http.get(this.baseUrl+'/'+id));
    
        return this.http.get(this.baseUrl+'/'+id);
          
        
        
          }
        U1(voituremarque:String):Observable<any>{
          console.log(voituremarque);
          console.log( this.http.get(this.baseUrl+'/v'+voituremarque));
    
        return this.http.get(this.baseUrl+'/v'+voituremarque);
          
        
        
          }
    getv():Observable<any>{
      return this.http.get(this.baseUrl+'/uu');
      
    
      
    
      ;}
    getUsers2():Observable<any>{
      return this.http.get(this.baseUrl+'/users3');
      
    
    
      ;}
     
    getUser(id:Number){
      return this.http.get(this.baseUrl+'/user/'+id);
    
    
    
      ;}
      createUserLog(user:User){
        return this.http.post(this.baseUrl+'/user2Log',user);
      }

      createUserLog1():Observable<any>{
        //return this.http.post(this.baseUrl+'/api1/emailemail',email) ;
        return this.http.get(this.baseUrl+'/usersh') ;
      }


      createUserLog1l(email:String):Observable<any>{
        return this.http.post(this.baseUrl+'/api1/emailemailemailj',email) ;

      }


      createUserLog2dash(email:String):Observable<any>{
        return this.http.post(this.baseUrl+'/api1/email2',email) ;

      }

      createUserprofil(email:String):Observable<any>{
        return this.http.post(this.baseUrl+'/api1/emailemailemail',email) ;

      }
      createUserLog2(email:String):Observable<any>{
        return this.http.post(this.baseUrl+'/emailemailemail',email) ;

      }
      createUserLogProfile(email:String):Observable<any>{
        return this.http.post(this.baseUrl+'/email11',email) ;

      }


      createUser(user:User){
        return this.http.post(this.baseUrl+'/user2',user);
      }
      createdemande(demande:Demande){
       
        return this.http.post(this.baseUrl+'/api3/demande',demande);
      }

      createdemandeid(email:string){
        return this.http.post(this.baseUrl+'/api3/demande1',email );

      }
      //createdemandeid2(email:string ,id:number){
       // return this.http.post(this.baseUrl+'/api3/demande2/?userId='+id,email  );

      //}

      createUser2(voiture:Voiture){
       
        return  this.http.post('http://localhost:8095/api/user4',voiture);
      }
      createUsero(demande:Demande,user2Id){
        this.demande1=demande;
        console.log(this.demande1);
        return this.http.post(this.baseUrl+'/1?user2Id='+user2Id ,this.demande1);
        
    
      }
      createUserabc(demande:Demande,user2Id){
        this.demande1=demande;
        console.log(this.demande1+"demmmmande11111111111111111111");
        console.log(user2Id+"user222222222222222222id")
        return this.http.post(this.baseUrl+'a/11?user2Id='+user2Id ,this.demande1);
        
    
      }


       createUserLogin(user:User){
         console.log(user.email +'heeeeeey');
         if (this.http.post('http://localhost:8095/7/user27',user)){
           console.log('truuuue');
         this.http.post('http://localhost:8095/7/user27',user);
        return (console.log('tru'))}



      }
      createF(user2Id:number){
        return this.http.get(this.baseUrl+'/uu?userId='+user2Id);
      

      }
      
      cr(id:number){
        console.log(id);
        return this.http.get(this.baseUrl+'/user/'+id);
      

      }
      //cr1(User:user5){
        //console.log(user5);
        //return this.http.put(this.baseUrl+'/user/cr1',user5);
      

      //}
      
      
     
     



      deleteUser(id:Number){
        return this.http.delete(this.baseUrl+'/'+id);
      
      
      
        ;}
      
        updateUser(user:User){
         return this.http.put(this.baseUrl+'/users3',user);

        }
    
        deletedemande(id:Number){
          return this.http.delete(this.baseUrl+'a/'+id);
        
        
        
          ;}
          deletedemande1(id:Number){
            return this.http.post(this.baseUrl+'/aa',id);
          
          
          
            ;}
        
          updatedemande(demande:Demande){
           return this.http.put(this.baseUrl+'a/',demande);
  
          }

          updatedemande111(demande:Demande){
            return this.http.put(this.baseUrl+'a/users3',demande);
   
           }
   
         
      
        
        
        
          updateUser2(id:Number){
            return this.http.get(this.baseUrl+'/user3'+id);
          
          
          
            ;}


            getUsersmd(){
              return this.http.get(this.baseUrl+'/usersh');
            }








            getUsers2019():Observable<any>{

            return this.http.get(this.baseUrl + '/users2019');
           


            }
            createUserpublication(message:Message){
              return this.http.post(this.baseUrl+'/user2p',message);
            }

/********************************019/08/2019********** */
user2publication(message:Message){
  return this.http.put(this.baseUrl+'/users2save',message);
}



user2all():Observable<any>{
  return this.http.get(this.baseUrl+'/user2all');

  

  ;}

/********************************************************** */


/*******************20/08/2019******************************** */

user2Idm(user2Id):Observable<any>{
  return this.http.get(this.baseUrl+'/user2Idm'+user2Id);
}
/************************************************************* */















            setter(user:User){
              console.log(user);
              this.user1=user;
            }


            setter1(demande:Demande){
              this.demande=demande;
            }

            setter22(demande:Demande , ind:any){
              this.demande=demande;
              this.id1=ind;
            }

            setter11(ind:any){
              this.id1=ind;
            }

            setter33(voiture:Voiture){
              this.voiture=voiture;
            }

            setter44(email:any){
              this.email=email;
            }
            getter(){
              console.log(this.user1+"eeee");
              return this.user1;
            }
            getter44(){
              console.log(this.email+"eeee");
              return this.email;
            }
            getterdemande(){
              return this.demande;
            }
            getterid(){
              return this.id1;
            }
            setter2(user2:User2){
              this.user2=user2;
            }
            setter3(voiture:Voiture){
              this.voiture=voiture;
            }

            settero(voitureo:Voiture){
              this.voitureo=voitureo;
            }



            logout(){

              localStorage.removeItem('tohen');
            }
  
}
